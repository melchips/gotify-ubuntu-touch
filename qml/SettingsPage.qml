import QtQuick 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

Page {
    property string error: ''
    property string version: ''

    header: PageHeader {
        id: header
        title: i18n.tr('Settings')

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                }
            ]
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                rightMargin: units.gu(1)
                leftMargin: units.gu(1)
                topMargin: units.gu(2)
            }

            spacing: units.gu(2)

            Label {
                text: i18n.tr('Gotify Server URL')
                textSize: Label.Large
            }

            TextField {
                Layout.fillWidth: true

                onTextChanged: settings.url = text
                Component.onCompleted: text = settings.url

                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhUrlCharactersOnly
            }

            Label {
                text: i18n.tr('App Token')
                textSize: Label.Large
            }

            TextField {
                Layout.fillWidth: true

                onTextChanged: settings.appToken = text
                Component.onCompleted: text = settings.appToken

                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
            }

            Label {
                text: i18n.tr('App token for sending messages')
            }

            Label {
                text: i18n.tr('Client Token')
                textSize: Label.Large
            }

            TextField {
                Layout.fillWidth: true

                onTextChanged: settings.clientToken = text
                Component.onCompleted: text = settings.clientToken

                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
            }

            Label {
                text: i18n.tr('Client token for reading messages')
            }

            Button {
                text: i18n.tr('Verify url and tokens')
                color: UbuntuColors.green

                onClicked: {
                    client.getVersion(function(err, data) {
                        if (err) {
                            error = err;
                            version = '';
                        }
                        else {
                            error = '';
                            version = data.version;
                        }
                    })
                }
            }

            Label {
                visible: error
                text: error
                color: UbuntuColors.red
                wrapMode: Label.WordWrap
            }

            Label {
                visible: version
                text: i18n.tr('Found Gotify version:') + ' ' + version
            }

            /*
            // TODO make this have a copy to clipboard button
            Label {
                visible: pushClient.token
                text: i18n.tr('Push client token:')
            }

            Label {
                Layout.fillWidth: true

                visible: pushClient.token
                text: pushClient.token
                wrapMode: Label.WrapAnywhere
            }
            */
        }
    }
}
